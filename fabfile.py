# --coding: utf8--
import os

try:
    from kupola.fabric_class import DjangoFabric,\
        add_class_methods_as_functions
except ImportError:
    def add_class_methods_as_functions(*args):
        pass

    class DjangoFabric():
        pass
from fabric.api import local, sudo
from fabric.context_managers import settings


class MediaFabric(DjangoFabric):
    host = '192.168.1.175'
    app_name = 'media_server'
    repository = 'git@repo.kupo.la:kupola/media.git'
    remote_db_name = 'media'
    local_db_name = 'media'
    user = 'deployer'
    skip_db_dump = True
    skip_tests = True

    def fab_copy_presets_images(self):
        u"""
            Скопировать файлы для пресетов в media/presets
        """
        remote_django_settings = self.get_remote_django_settings()

        rp = os.path.join(remote_django_settings.MEDIA_ROOT, 'presets')
        lp = os.path.join(
            self.get_local_project_path(), 'media', 'presets')

        with settings(warn_only=True):
            local('scp -r {} {}@{}:{}'.format(
                lp,
                self.user,
                self.host,
                rp
            ))

    def fab_deploy(self):
        if not self.skip_tests:
            self.fab_test()

        self.remote_create_project_paths()
        self.remote_clone_repository()

        if not self.skip_db_dump:
            self.fab_remote_dumpdb()

        self.fab_push()
        self.fab_remote_update_repository()
        self.fab_remote_del_pyc()
        self.fab_remote_pip_whell()
        self.fab_link_local_settings()
        self.fab_remote_manage('migrate')
        self.fab_remote_manage('bower_install')
        self.fab_remote_manage('collectstatic --noinput')
        self.fab_copy_presets_images()
        sudo('touch {0}/reload.me'.format(self.get_remote_venv_path()))

    @classmethod
    def update_id_seq():
        u"""
            после загрузки старых данных нужно обновить индекс id в таблицах
            media_admin_image и media_admin_positiontag следующим sql

            WITH mx AS ( SELECT MAX(id) AS id FROM media_admin_positiontag)
            SELECT setval('media_admin_positiontag_id_seq', mx.id) AS curseq
            FROM mx;
        """
        pass

    @classmethod
    def create_mysql_db(user, dbname):
        u"""
            echo "CREATE DATABASE IF NOT EXISTS old_media" | mysql -u root -p
        """
        pass

    @classmethod
    def restore_from_dump(user, dbname, backup):
        u"""
            mysql -u root -p -h localhost -D old_media
             -o < /var/venv/media/media_zk_online_ru.sql
        """
        pass

__all__ = add_class_methods_as_functions(MediaFabric(), __name__)
