# --coding: utf8--
from locust import HttpLocust, TaskSet
import urllib


def tags(l):
    l.client.get(
        urllib.quote(
            "/embed-media/tags/дракино,коттедж/100x64-pv3,link,description/"))


def media_by_id(l):
    l.client.get("/embed-media/id/39060/100x100-zk,link,description/")


def link(l):
    pass


class UserBehavior(TaskSet):
    tasks = {media_by_id: 1}


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    # min_wait = 5000
    min_wait = 1000
    max_wait = 7000
