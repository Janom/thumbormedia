# --coding: utf8--

from rest_framework.exceptions import ValidationError
from rest_framework import serializers
from media_admin.models import Image
from lib.presets import PRESETS


class ImageAPISerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = (
            'id',
            'title',
            'alt',
            'orig',
            'preset',
            'thumb',
        )

    preset = serializers.SerializerMethodField()
    thumb = serializers.SerializerMethodField()
    orig = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(ImageAPISerializer, self).__init__(*args, **kwargs)
        self.preset_name = self.context['view'].preset

        if self.preset_name and not PRESETS.get(self.preset_name, None):
            raise ValidationError(
                u"Такого пресета нет: {}".format(self.preset_name),
            )

    def get_preset(self, obj):
        return self.preset_name if self.preset_name else None

    def get_thumb(self, obj):
        return obj.thumb(
            PRESETS[self.preset_name]
        ) if self.preset_name else None

    def get_orig(self, obj):
        return obj.thumb(PRESETS['orig'])
