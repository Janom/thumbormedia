# --coding: utf8--
from media_admin.models import Image
from rest_framework import viewsets
from rest_framework.pagination import LimitOffsetPagination
from api_v_1.serializers import ImageAPISerializer


class DefaultLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 10


class ImagesViewSet(viewsets.ReadOnlyModelViewSet):
    allowed_methods = ['GET']
    queryset = Image.objects.filter()
    serializer_class = ImageAPISerializer
    pagination_class = DefaultLimitOffsetPagination

    def dispatch(self, request, *args, **kwargs):
        self.tags = filter(len, request.REQUEST.get('tags', '').split(','))
        self.ids = filter(len, request.REQUEST.get('ids', '').split(','))
        self.preset = request.REQUEST.get('preset')
        return super(ImagesViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = self.queryset
        if self.ids:
            queryset = queryset.filter(pk__in=self.ids)
        if self.tags:
            for tag in self.tags:
                queryset = queryset.filter(tags__text=tag.strip())
        return queryset
