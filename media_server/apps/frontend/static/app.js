/*global angular */
'use strict';

var app = angular.module('app', ['ngSanitize', 'MassAutoComplete', 'ui.bootstrap', 'flow']).
config(['flowFactoryProvider', '$httpProvider',
    function (flowFactoryProvider, $httpProvider) {
        flowFactoryProvider.defaults = {
            target: '/admin/api/v1.0/upload',
            permanentErrors: [403, 500, 501],
            maxChunkRetries: 2,
            chunkRetryInterval: 5000,
            simultaneousUploads: 2,
            testChunks: true,
            chunkSize: 1000000000000,
            supportDirectory: true,
        };

        // flowFactoryProvider.on('catchAll', function (event) {
        //     console.log('catchAll', arguments);
        // });

        // csrf
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    }
]);

app
.controller('AddParamsCtrl', AddParamsCtrl)
.controller('ModalInstanceCtrl', ModalInstanceCtrl);

function AddParamsCtrl($scope, $http, $sce, $q, $modal, $templateCache) {
    $scope.config = {
        query: function (flowFile, flowChunk) {
            return {
                _title: flowFile._title || '',
                tags: flowFile.tags || ''
            };
        }
    };

    $http.get('/admin/api/v1.0/tags', {}).
        success(function(data, status, headers, config) {
            $scope.tags = data;
    });

    function suggest_tags(term) {
        var q = term.toLowerCase().trim();
        var results = [];
        // Find first 10 states that start with `term`.
        for (var i = 0; i < $scope.tags.length && results.length < 10; i++) {
          var state = $scope.tags[i];
          if (state.toLowerCase().indexOf(q) === 0)
            results.push({ label: state, value: state });
        }
        return results;
    }

    function suggest_state_delimited(term) {
        var ix = term.lastIndexOf(','),
        lhs = term.substring(0, ix + 1),
        rhs = term.substring(ix + 1),
        suggestions = suggest_tags(rhs);
        suggestions.forEach(function (s) {
            s.value = lhs + s.value;
        });
        return suggestions;
    };

    $scope.autocomplete_options = {
        suggest: suggest_state_delimited
    };

    $scope.getName = function(name) {
        return name.substr(0, name.lastIndexOf('.')).capitalizeFirstLetter();
    };

    $scope.copyTopTags = function(index, tags) {
        if ($scope.$flow.files[index-1]){
            $scope.$flow.files[index].tags = JSON.parse(JSON.stringify($scope.$flow.files[index-1].tags));
        }
    }

    $scope.copyTags = function(index, tags) {
        // дублировать ключевые слова во все файлы идущие за текущим
        var len = $scope.$flow.files.length;
        for(var i=1; i<len; i++){
           if (i>index){
               $scope.$flow.files[i].tags = JSON.parse(JSON.stringify(tags));
           } 
        }
    }

    $scope.is_errors = function(){
        // проверка наличия ошибок при передаче файлов
        for(var i=0; i<$scope.$flow.files.length; i++){
            if($scope.$flow.files[i].error){
                return true
            }
        }
        return false
    }

    $scope.retry_all = function() {
        each($scope.$flow.files, function (file) {
            file.retry()
        })

        $scope.$on('flow::complete', function (event, $flow) {
            if ((!$scope.is_errors()) && (!$scope.modal_opened)) {
                $scope.open()
            }
        })

    }

    $scope.modal_opened = false;

    $scope.open = function () {     
        $scope.modal_opened = true
        var modalInstance = $modal.open({
            template: '' +
                "<div class='modal-content'>" +
                "  <div class='modal-header'>" +
                "    <h3 class='modal-title'>Все файлы сохранены.</h3>" +
                "  </div>" +
                "  <div class='modal-body'>" +
                "    <p class='lead'>" +
                "      <a href='/admin/media_admin/image/'>Перейти к списку изображений</a></p>" +
                "    <p class='lead'><a href='./'>Загрузить ещё</a></p>" +
                "  </div>" +
                "  <div class='modal-footer'>" +
                "    <button class='btn' ng-click='cancel()'>Закрыть</button>" +
                "  </div>" +
                "</div>",
            controller: 'ModalInstanceCtrl',
        })
    }
}

function ModalInstanceCtrl($scope, $modalInstance, $log) {
    $scope.cancel = function () {
        $modalInstance.close('close')
        $scope.modal_opened = false
    }
}

//---------------------далее утилиты и вспомогательные функции------------------
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1)
}


function each(obj, callback, context) {
    if (!obj) {
        return
    }
    var key;
    // Is Array?
    if (typeof(obj.length) !== 'undefined') {
      for (key = 0; key < obj.length; key++) {
        if (callback.call(context, obj[key], key) === false) {
          return
        }
      }
    } else {
      for (key in obj) {
        if (obj.hasOwnProperty(key) && callback.call(context, obj[key], key) === false) {
          return
        }
      }
    }
}

// для отладки
app.filter("print_all_keys", function(){
    return function(input){
        return Object.keys(input)
    }
})