# --coding: utf8--

from django.conf.urls import url, patterns
from legacy import views

urlpatterns = patterns(
    '',
    url(r'^embed-presets', views.embed_presets, name='embed_presets'),
    url(
        r'^embed-media/id/(?P<pk>\d+)/(?P<preset_params>.*)',
        views.LegacyMedia.as_view(),
        name='legacy_media_by_old_id_with_preset'
    ),
    url(
        r'^embed-media/id/(?P<pk>\d+)',
        views.LegacyMedia.as_view(),
        name='legacy_media_by_old_id'
    ),
    url(
        r'^embed-media/tags/(?P<tags>[\w,]+)/(?P<preset_params>.*)',
        views.LegacyMediaTags.as_view(),
        name='legacy_media_by_tags_with_preset'
    ),
)
