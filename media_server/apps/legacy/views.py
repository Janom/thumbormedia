# --coding: utf8--
from django.views.generic import DetailView, ListView
from django import http
from media_admin.models import Image
from lib.presets import PRESETS
from exceptions import NotImplementedError


def embed_presets(request):
    presets_names = "\n".join(PRESETS.keys())
    return http.HttpResponse(
        presets_names,
        status=200,
        content_type='text/plain')


class Legacy(object):
    u"""
        включен метод dispatch, который забирает параметры из url касаемо
        пресета
    """
    template_params = {
        'title': False,
        'description': False,
        'link': False,
        'preset': None,
        'preset_name': None
    }

    def dispatch(self, request, *args, **kwargs):
        _template_params = filter(
            len,
            kwargs.get('preset_params', '').split(',')
        )

        if _template_params:
            if 'title' in _template_params:
                self.template_params['title'] = True
            if 'description' in _template_params:
                self.template_params['description'] = True
            if 'link' in _template_params:
                self.template_params['link'] = True

        if _template_params:
            if _template_params[0] in PRESETS:
                self.template_params['preset'] = PRESETS[_template_params[0]]
                self.template_params['preset_name'] = _template_params[0]
            else:
                raise NotImplementedError(
                    'preset не реализован и или не сконфигурирован')
        else:
            self.template_params['preset'] = PRESETS['orig']
            self.template_params['preset_name'] = 'orig'

        return super(Legacy, self).dispatch(request, *args, **kwargs)


class LegacyMediaTags(Legacy, ListView):
    template_name = "legacy/media_list.html"
    model = Image
    context_object_name = 'images'

    def get_queryset(self):
        if self.tags:
            base_query = Image.objects.filter()
            for tag in self.tags:
                base_query = base_query.filter(tags__text=tag.strip())
            return base_query
        else:
            return []

    def dispatch(self, request, *args, **kwargs):
        self.tags = filter(len, kwargs.get('tags', '').split(','))
        return super(LegacyMediaTags, self).dispatch(request, *args, **kwargs)


class LegacyMedia(Legacy, DetailView):
    model = Image
    template_name = "legacy/media.html"
