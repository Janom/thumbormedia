# --coding: utf8--
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from media_admin.models import Image, PositionTag
from django_thumbor import generate_url


class PositionTagAdmin(admin.ModelAdmin):
    list_display = ('text', 'id')
    search_fields = ('text', )
    ordering = ('-id',)


class TraversalTagsListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _(u'Уточняющие ключевые слова')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'traversal_tags'

    def lookups(self, request, model_admin):
        u"""
        """
        first_tag = request.GET.get('tags__text')
        if first_tag:
            images = Image.objects.filter(tags__text__in=[first_tag])
            traversal_tags = []
            for i in images:
                for j in i.tags.all():
                    if j not in traversal_tags and j.text != first_tag:
                        traversal_tags.append(j)

            return ((t.text, t.text) for t in traversal_tags)
        else:
            return ()

    def queryset(self, request, queryset):
        u"""
        """
        traversal_tag = request.GET.get('traversal_tags')
        first_tag = request.GET.get('tags__text')
        if traversal_tag and first_tag:
            return queryset.filter(tags__text__in=[traversal_tag])
        else:
            return queryset


class ImageAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def get_tags(self, obj):
        return ", ".join([t.text for t in obj.tags.all()])
    get_tags.short_description = u'Ключевые слова'

    def media_id(self, obj):
        return "[media: {}]".format(obj.id)
    media_id.short_description = u'Медиа ID'

    def display_image(self, obj):
        thumb_url = generate_url(obj.url, width=100)
        return '<img src="{0}"/>'.format(thumb_url)
    display_image.short_description = u'Изображение'
    display_image.allow_tags = True

    ordering = ('-updated', '-id')
    list_display = (
        'media_id',
        'display_image',
        'title',
        'get_tags',
        'updated'
    )
    search_fields = ('tags__text', 'description', 'original_filename')
    list_filter = (TraversalTagsListFilter, 'tags__text', )
    readonly_fields = ('total_size', 'state', 'path', )
    filter_horizontal = ('tags',)

    fieldsets = (
        ('', {
            'fields': (
                'state',
                'tags',
                'title',
                'description')}),
        ('properties', {
            'fields': (
                'id',
                'total_size',
                'state',
                'path'
                ),
            }),
    )


class UploadImageProxyModel(Image):
    class Meta:
        proxy = True
        verbose_name = _(u'загрузить изображение')
        verbose_name_plural = _(u'загрузить изображения')
        ordering = ("id", )


class UploadImageAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    # def has_change_permission(self, request):
    #     return False

    change_list_template = 'media_admin/upload.hamlpy'


admin.site.register(UploadImageProxyModel, UploadImageAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(PositionTag, PositionTagAdmin)
