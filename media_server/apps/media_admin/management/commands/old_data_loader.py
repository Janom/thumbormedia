# coding: utf-8
import os
import datetime
import time
import hashlib
from django.core.management.base import BaseCommand
from django.conf import settings
import pymysql
from unidecode import unidecode
from media_admin.models import PositionTag, Image


class Command(BaseCommand):
    u"""
        стоит быть внимательным при запуске команды
        правильно указать путь к старым файлам
        и учесть что суммарный объем около 5 гб
    """
    can_import_settings = True
    cnx = pymysql.connect(**settings.OLD_DB_CONFIG)

    def cursor(self):
        return self.cnx.cursor()

    def upload_tags(self):
        u"""
            загружаем теги
        """
        self.cnx = pymysql.connect(**settings.OLD_DB_CONFIG)
        query = ("SELECT tid, name FROM term_data")
        cursor = self.cnx.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        cursor.close()
        self.cnx.close()
        for old_id, name in rows:
            PositionTag(
                text=name,
                id=old_id
            ).save()

    def upload_files(self):
        u"""
            загружаем файлы
        """
        def get_path(line, prefix='sites/media.zk-online.ru/'):
            if line.startswith(prefix):
                return line[len(prefix):]
            else:
                return line

        def convert(timestamp):
            u"""перевод даты из timestamp в datetime объект"""
            return datetime.datetime.fromtimestamp(timestamp)

        def upload_tree_path(base_folder='.', now=datetime.datetime.now()):
            return "{}/{}/{}/{}".format(
                    base_folder,
                    now.year,
                    now.month,
                    now.day,
                    )

        query = """
        select c.title, c.nid, a.filepath, a.filename, c.changed
        from
            files a
                inner join
            content_type_img b
                on a.fid = b.field_image_fid
                inner join
            node c
                on b.vid = c.vid"""

        self.cnx = pymysql.connect(**settings.OLD_DB_CONFIG)
        cursor = self.cnx.cursor()
        cursor.execute(query)
        for title, nid, filepath, filename, changed in cursor:
            p = os.path.join(settings.TMP_OLD_MEDIA, get_path(filepath))
            try:
                f = open(p, 'r')
                im = Image(
                    id=nid,
                    title=title,
                    state=1,
                    folder=upload_tree_path('old_images', convert(changed)),
                    original_filename=filepath.split('/')[-1],
                    identifier='{}-{}'.format(
                        hashlib.md5(str(time.time())).hexdigest(),
                        unidecode(filename))[:200]
                )
                im.force_save_file(f)
            except IOError:
                print "path: {} doesn't exist".format(p)

        cursor.close()
        self.cnx.close()

    def link_tags(self):
        self.cnx = pymysql.connect(**settings.OLD_DB_CONFIG)
        query = ("SELECT tid, nid FROM term_node")
        cursor = self.cnx.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        self.cnx.close()
        for tid, nid in rows:
            try:
                tag = PositionTag.objects.get(pk=tid)
                image = Image.objects.get(pk=nid)
            except PositionTag.DoesNotExist, Image.DoesNotExist:
                tag = image = None
                print "{tid}-{nid} doens not exist".format(tid, nid)
            except Exception as e:
                print e
            if image and tag:
                image.tags.add(tag)
                image.save()

    def handle(self, *args, **options):
        self.upload_tags()
        # self.upload_files()
        # self.link_tags()
        pass
