# coding: utf-8
import os
import threading
from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.core.files.storage import default_storage
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

from lib.common.models import DateCreateUpdateModel
from signals import file_is_ready, file_joining_failed
from lib.common.image_upload import upload_tree_path, chunk_upload_to, \
    make_sure_path_exists
from django_thumbor import generate_url


class PositionTag(models.Model):
    class Meta:
        verbose_name = _(u'ключевое слово')
        verbose_name_plural = _(u'ключевые слова')
        ordering = ("id", )

    text = models.CharField(
        verbose_name=_(u"ключевое слово"), max_length=255)

    def __unicode__(self):
        return self.text


class FlowjsFile(models.Model):
    u"""
        файл загруженный с использованием flowjs (абстрактный класс)
    """

    class Meta:
        abstract = True

    STATE_UPLOADING = 1
    STATE_COMPLETED = 2
    STATE_UPLOAD_ERROR = 3
    STATE_JOINING = 4
    STATE_JOINING_ERROR = 5
    STATE_SAVE_FILE_ERROR = 5

    STATE_CHOICES = [
        (STATE_UPLOADING, "Uploading"),
        (STATE_COMPLETED, "Completed"),
        (STATE_UPLOAD_ERROR, "Upload Error"),
        (STATE_JOINING, "Joining chunks"),
        (STATE_JOINING_ERROR, "Joining chunks error"),
        (STATE_SAVE_FILE_ERROR, "Save file error"),
    ]
    folder = models.CharField(
        verbose_name=_(u"древовидный путь к файлу ./гггг/мм/дд"),
        default=upload_tree_path('images'),
        max_length=1024)

    # идентификатор и файл изображения
    identifier = models.SlugField(max_length=255, unique=True, db_index=True)
    original_filename = models.CharField(max_length=200)
    final_file = models.ImageField(
        upload_to=chunk_upload_to,
        verbose_name=_(u"оригинал изображения"),
        null=True, blank=True,
        max_length=1024)
    total_size = models.IntegerField(default=0)
    total_chunks = models.IntegerField(default=0)

    # текущее состояние файла
    total_chunks_uploaded = models.IntegerField(default=0)
    state = models.IntegerField(
        choices=STATE_CHOICES, default=STATE_UPLOADING)

    def __unicode__(self):
        return self.identifier

    def update(self):
        u"""
            total_chunks_uploaded = 1
        """
        if self.total_chunks == 1:
            super(FlowjsFile, self).save()
        else:
            self.total_chunks_uploaded = self.chunks.count()
            super(FlowjsFile, self).save()
            self.join_chunks()

    @property
    def extension(self):
        name, ext = os.path.splitext(self.original_filename)
        return ext

    @property
    def filename(self):
        u"""
            уникальное имя основанное на идентификаторе
        """
        return '%s%s' % (self.identifier, self.extension)

    @property
    def file(self):
        u"""
            загруженный файл
        """
        if self.state == self.STATE_COMPLETED:
            return default_storage.open(self.path)
        return None

    @property
    def path(self):
        make_sure_path_exists(self.catalog)
        return os.path.join(
            settings.FLOWJS_UPLOAD_PATH, self.folder, self.filename)

    @property
    def catalog(self):
        return os.path.join(settings.FLOWJS_UPLOAD_PATH, self.folder)

    @property
    def url(self):
        return os.path.join(
            settings.MEDIA_URL,
            self.folder,
            self.filename
        )

    def get_chunk_filename(self, number):
        u"""
            название куска файла (chunk)
        """
        return '%s-%s.tmp' % (self.identifier, number)

    @property
    def join_in_background(self):
        u"""
            проверка должен ли файл быть склеен в selery как задача
            или операция должна пройти сразу
        """
        # TODO realize background join
        return False

    def force_save_file(self, file):
        u"""
            быстрое сохранение файла, без склейки
            используется если файл состоит из одной части

        """
        try:
            temp_file = default_storage.open(self.path, 'wb')
            temp_file.write(file.read())
            temp_file.close()
            self.final_file = self.path
            self.state = self.STATE_COMPLETED
        except Exception, e:
            print e
            self.state = self.STATE_SAVE_FILE_ERROR

        super(FlowjsFile, self).save()

    def join_chunks(self):
        u"""
            собрать файл
        """
        if self.state == self.STATE_UPLOADING \
                and self.total_chunks_uploaded == self.total_chunks:
            if self.join_in_background:
                self.state = self.STATE_JOINING
                super(FlowjsFile, self).save()
                if settings.FLOWJS_WITH_CELERY:
                    from tasks import join_chunks_task
                    join_chunks_task.delay(self)
                else:
                    t = threading.Thread(target=self._join_chunks)
                    t.setDaemon(True)
                    t.start()
            else:
                self._join_chunks()

    def _join_chunks(self):
        try:
            temp_file = default_storage.open(self.path, 'wb')
            for chunk in self.chunks.all():
                chunk_bytes = chunk.file.read()
                temp_file.write(chunk_bytes)
            temp_file.close()
            self.final_file = self.path
            self.state = self.STATE_COMPLETED
            super(FlowjsFile, self).save()

            # send ready signal
            if self.join_in_background:
                file_is_ready.send(self)

            self.delete_chunks()

        except Exception:
            self.state = self.STATE_JOINING_ERROR
            super(FlowjsFile, self).save()

            # send error signal
            if self.join_in_background:
                file_joining_failed.send(self)

    def delete_chunks(self):
        # if settings.FLOWJS_WITH_CELERY:
        #     from tasks import delete_chunks_task
        #     delete_chunks_task.delay(self)
        # else:
        t = threading.Thread(target=self._delete_chunks)
        t.setDaemon(True)
        t.start()

    def _delete_chunks(self):
        for chunk in self.chunks.all():
            chunk.delete()


class Image(FlowjsFile, DateCreateUpdateModel):
    class Meta:
        verbose_name = _(u'изображение')
        verbose_name_plural = _(u'изображения')
        ordering = ("id", )

    tags = models.ManyToManyField(
        PositionTag, blank=True,
        related_name="tag_in_images",
        verbose_name=_(u"ключевые слова"))

    description = models.TextField(
        _(u"описание"), null=True, blank=True)

    title = models.TextField(
        _(u"название для изображения"), null=True, blank=True)

    def __unicode__(self):
        return self.url

    def alt(self):
        return self.title

    def thumb(self, preset):
        thumb_url = generate_url(
            self.url,
            width=preset.width,
            height=preset.height,
            filters=preset.filters
        )
        return thumb_url


class FlowjsFileChunk(models.Model):
    u"""
        chunk - часть загружаемого файла
    """

    parent = models.ForeignKey(Image, related_name="chunks")
    file = models.FileField(max_length=255, upload_to=chunk_upload_to)
    number = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    folder = models.CharField(
        verbose_name=_(u"древовидный путь к файлу ./гггг/мм/дд"),
        default=upload_tree_path('images'),
        max_length=1024)

    def __unicode__(self):
        return self.filename

    @property
    def filename(self):
        return self.parent.get_chunk_filename(self.number)

    def save(self, *args, **kwargs):
        super(FlowjsFileChunk, self).save(*args, **kwargs)
        self.parent.update()


@receiver(pre_delete, sender=Image)
def flow_file_delete(sender, instance, **kwargs):
    """
        Удалять файл из файловой системы
        при удалении объекта Image
    """
    if settings.FLOWJS_REMOVE_FILES_ON_DELETE:
        default_storage.delete(instance.path)


@receiver(pre_delete, sender=FlowjsFileChunk)
def flow_file_chunk_delete(sender, instance, **kwargs):
    """
        Удалить загруженные части файла
    """
    instance.file.delete(False)
