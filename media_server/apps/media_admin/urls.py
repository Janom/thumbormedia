# --coding: utf8--

from django.conf.urls import url, patterns
from media_admin import views
from django.contrib import admin
from django.contrib.sites.models import Site

admin.autodiscover()
admin.site.unregister(Site)


urlpatterns = patterns(
    '',
    url(
        r'^admin/api/v1.0/upload',
        views.UploadApiView.as_view(),
        name='upload_api'
    ),
    url(
        r'^admin/api/v1.0/tags',
        views.PositionTagApiView.as_view(),
        name='tags'
    ),
)
