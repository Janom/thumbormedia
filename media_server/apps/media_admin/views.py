# --coding: utf8--
from django import http
from django import forms
from django.views.generic import View
from lib.common.api_helpers import JSONResponse, is_superuser
from media_admin.models import Image, FlowjsFileChunk, PositionTag
from django.utils.decorators import method_decorator
from unidecode import unidecode


class FlowFileForm(forms.Form):
    file = forms.FileField()


class PositionTagApiView(View):
    u"""
        Все ключевые слова
    """
    @method_decorator(is_superuser)
    def dispatch(self, request, *args, **kwargs):
        return super(PositionTagApiView, self).dispatch(
            request, *args, **kwargs)

    def get(self, *args, **kwargs):
        return JSONResponse(
            PositionTag.objects.all().values_list('text', flat=True)
        )


def get_unidecode_file_name(name):
    u"""
        получить название файла без расширения
    """
    return unidecode(
        name).replace(' ', '-').split('.')[0]


class UploadApiView(View):
    u"""
        первичная загрузка изображения
    """
    @method_decorator(is_superuser)
    def dispatch(self, request, *args, **kwargs):
        # get flow variables
        self.flowChunkNumber = int(request.REQUEST.get('flowChunkNumber'))
        self.flowChunckSize = int(request.REQUEST.get('flowChunkSize'))
        self.flowCurrentChunkSize = int(
            request.REQUEST.get('flowCurrentChunkSize'))
        self.flowTotalSize = int(request.REQUEST.get('flowTotalSize'))
        self.flowIdentifier = request.REQUEST.get('flowIdentifier')
        self.flowFilename = request.REQUEST.get('flowFilename')
        self.flowRelativePath = request.REQUEST.get('flowRelativePath')
        self.flowTotalChunks = int(request.REQUEST.get('flowTotalChunks'))

        self.title = request.REQUEST.get('_title')
        self.tags = request.REQUEST.get('tags')
        if self.tags:
            self.tags = filter(len, self.tags.split(','))

        # identifier is a combination of flow identifier and file name
        self.identifier = ('{}-{}'.format(
            self.flowIdentifier.split('-')[0],
            get_unidecode_file_name(self.flowFilename)))

        return super(UploadApiView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        u"""
            проверка файла, перед отправкой,
            если файл загружен возвращаем идентификатор
            если нет возвращаем 400 ошибку
        """
        if not self.tags:
            return http.HttpResponse(
                u'Для "{}" не заполнены ключевые слова'.format(
                    self.title.lower()), status=403)

        try:
            FlowjsFileChunk.objects.get(
                number=self.flowChunkNumber,
                parent__identifier=self.identifier)
            return http.HttpResponse('object already exist', status=202)
        except FlowjsFileChunk.DoesNotExist:
            return http.HttpResponse('', status=404)

    def post(self, request, *args, **kwargs):
        u"""
            загрузка файла по частям
            если файл состоит всего из одной части сохранить его сразу
        """
        form = FlowFileForm(request.POST, request.FILES)
        if not form.is_valid():
            return http.HttpResponse(form.errors, status=200)
        if self.flowTotalChunks == 1:
            flow_file, created = Image.objects.get_or_create(
                identifier=self.identifier,
                defaults={
                    'original_filename': self.flowFilename,
                    'total_size': self.flowTotalSize,
                    'total_chunks': self.flowTotalChunks,
                })

            flow_file.force_save_file(form.cleaned_data['file'])
            self.update_add_info(flow_file)
            return http.HttpResponse('', status=200)

        else:
            flow_file, created = Image.objects.get_or_create(
                identifier=self.identifier,
                defaults={
                    'original_filename': self.flowFilename,
                    'total_size': self.flowTotalSize,
                    'total_chunks': self.flowTotalChunks,
                })
            self.update_add_info(flow_file)

            # avoiding duplicated chucks
            chunk, created = flow_file.chunks.get_or_create(
                number=self.flowChunkNumber,
                defaults={
                    'file': form.cleaned_data['file'],
                })

            if not created:
                chunk.file = form.file
                chunk.size = form.size
                chunk.save()

            return http.HttpResponse('', status=200)

    def update_add_info(self, image):
        u"""
            обновляет существующий объект (Image)
        """
        if self.title:
            image.title = self.title
        if self.tags:
            image.tags.clear()
            existing_tags = []
            for tag in self.tags:
                existing_tags.append(
                    PositionTag.objects.get_or_create(text=tag)[0])
            for tag in existing_tags:
                image.tags.add(tag)
        image.save()
