# coding: utf-8
from functools import wraps
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import SessionAuthentication


def is_superuser(f):
    @wraps(f)
    def wrapper(request, *args, **kwds):
        if request.user.is_superuser:
            return f(request, *args, **kwds)
        else:
            return JSONResponse('access denied', status=403)
    return wrapper


class JSONResponse(HttpResponse):
    u"""
        json рендер объекта response
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class UnsafeSessionAuthentication(SessionAuthentication):
    u"""
        для отладки забываем про csrf

        пример использования в class based view
        указываем настройку
        # authentication_classes = (UnsafeSessionAuthentication,)
    """
    def authenticate(self, request):
        http_request = request._request
        user = getattr(http_request, 'user', None)

        if not user or not user.is_active:
            return None

        return (user, None)
