# coding: utf-8
import math
import datetime
import os
import hashlib
import mimetypes
from unidecode import unidecode
from django.conf import settings
from django.utils.encoding import smart_str, smart_unicode
from django.utils.deconstruct import deconstructible
from django.template.defaultfilters import slugify
from StringIO import StringIO
try:
    from PIL import Image
except ImportError:
    import Image


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError:
        pass


def base_expand(val, base=36):
    u"""
        django - кодирование имен файлов
    """
    val = abs(val)
    trans_table = '0123456789abcdefghijklmnopqrstuvwxyz'
    res = ''

    while val != 0:
        res += trans_table[(int(val % base))]
        val = math.floor(val / base)
    return res[::-1]


def generate_hash(s):
    u"""
        использовать django base кодирование имен файла
        при пустом исходе вернуть md5 хешn из строки
    """
    s = smart_str(s).encode('utf-8').lower()
    h = base_expand(hash(s))
    if h:
        return h
    else:
        return hashlib.md5(s).hexdigest()


@deconstructible
class TreePath(object):
    u"""
        Пример использования:
            photo = models.ImageField(
                upload_to=TreePath('images', use_hash=False))
        Назначение:
            динамически создает путь сохранения файла
            для поля django модели вида:

            ./base (в примере images)/2015(год)/02(месяц)/12(день)

            а так же заменяет название файла на md5 хеш
            от первоначального названия
            при повторной загрузке такого же изображения
            к имени конечного файла добавляется хеш от предведущего
            названия, на выходе имя файла имеет вид:
            (322tlx1_l90R1Sa.jpg)
    """

    def __init__(self, folder, use_hash=True):
        self.folder = folder
        self.use_hash = use_hash

    def __call__(self, instance, filename):
        just_name, ext = os.path.splitext(filename)[:2]
        now = datetime.datetime.now()
        fn = unidecode(smart_unicode(just_name))
        if self.use_hash:
            fn = generate_hash(fn)
        else:
            fn = slugify(fn)

        return "{}/{}/{}/{}/{}{}".format(
            self.folder,
            now.year,
            now.month,
            now.day,
            fn,
            ext
        )


def upload_tree_path(base_folder='.'):
    u"""
        директория для рагрузки файла
    """
    now = datetime.datetime.now()
    return "{}/{}/{}/{}".format(
            base_folder,
            now.year,
            now.month,
            now.day,
            )


def chunk_upload_to(instance, filename):
    u"""
        Сохранять куски файла и сам файл по заданному пути
    """
    return os.path.join(
        settings.FLOWJS_UPLOAD_PATH,
        instance.folder,
        instance.filename)


def guess_mimetype(url):
    u"""
        mimetype файла
    """
    mimetype = mimetypes.guess_type(url)
    if not mimetype or not mimetype[0]:
        name, ext = os.path.splitext(url)
        mimetypes_dict = {
            '.m4v': ['video/x-m4v'],
            '.m4a': ['audio/x-m4a'],
        }
        mimetype = mimetypes_dict.get(ext, ['unknown/unknown'])
    return mimetype[0]


def is_valid_image(image):
    u'''
        проверка что файл действительно является картинкой
    '''
    file = StringIO(image.read())

    try:
        trial_image = Image.open(file)
        trial_image.load()

        if hasattr(file, 'reset'):
            file.reset()

        trial_image = Image.open(file)
        trial_image.verify()
    except Exception:
        return False

    return True
