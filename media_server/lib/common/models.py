# coding: utf-8
u"""
    вспомогательные классы моделей
"""

from django.utils.translation import ugettext as _
from django.db import models


class DateCreateUpdateModel(models.Model):
    u"""
        добавляет время создания и дату обновления в модель
    """
    created = models.DateTimeField(
        verbose_name=_(u"дата и время создания"),
        auto_now_add=True)
    updated = models.DateField(
        verbose_name=_(u"дата обновления"),
        auto_now=True)

    class Meta:
        abstract = True
