# --coding: utf8--
from django.conf import settings
import os


def add_filter(f):
    u"""
        декоратор добавляет фильтр вычесленный в функции к списку
        фильтров пресета
    """
    def deco(self, *args):
        _filter = f(self, *args)
        self._filters.append(_filter)
        return self
    return deco


class Preset(object):
    def __init__(self, width=None, height=None):
        self._width = width
        self._height = height
        self._filters = []

    @add_filter
    def watermark(self, *args, **kwargs):
        u"""
            убираем пробелы чтобы не перекодировать url
        """

        url = os.path.join(
            settings.THUMBOR_MEDIA_URL,
            args[0])
        return "watermark({},{})".format(url, ','.join(map(str, args[1:])))

    @add_filter
    def round_corner(self, *args, **kwargs):
        return "round_corner({})".format(','.join(map(str, args)))

    @add_filter
    def no_upscale(self, *args, **kwargs):
        return "no_upscale()"

    @property
    def filters(self):
        return self._filters

    @property
    def height(self):
        if "no_upscale()" in self._filters:
            return ''
        else:
            return self._height or ''

    @property
    def width(self):
        if "no_upscale()" in self._filters:
            return ''
        else:
            return self._width or ''


PRESETS = {
  '100': Preset(100),
  '100x100-zk': Preset(100, 100).watermark('presets/100x100-zk.png', 0, 0, 0),
  '100x64-ny': Preset(100, 64).watermark('presets/100x64-ny.png', 0, 0, 0),
  '100x64-pv3': Preset(100, 64).watermark('presets/100x64-pv3.png', 0, 0, 0),
  '100x70': Preset(100, 70),
  '110x160-pv3': Preset(100, 64).watermark('presets/110x160-pv3.png', 0, 0, 0),
  '113': Preset(113),
  '120x120': Preset(120, 120),
  '120x170-zk': Preset(120, 170).watermark('presets/120x170-zk.png', 0, 0, 0),
  '120x80-zk': Preset(120, 80).watermark('presets/120x80-zk.png', 0, 0, 0),
  '123x95-sh': Preset(123, 95).watermark('presets/123x95-sh.png', 0, 0, 0),
  '128x180': Preset(128, 180),
  '140x100-pv3': Preset(140, 100).watermark(
    'presets/140x100-pv3.png', 0, 0, 0),
  '140x100-pv3-zoom': Preset(140, 100).watermark(
    'presets/140x100-pv3-zoom.png', 0, 0, 0),
  '140x110': Preset(140, 110),
  '140x89-ny': Preset(140, 89).watermark('presets/140x89-ny.png', 0, 0, 0),
  '142x102-dr': Preset(142, 102).watermark('presets/142x102-dr.png', 0, 0, 0),
  '143x107-tb': Preset(143, 107).watermark('presets/143x107-tb.png', 0, 0, 0),
  '147x190-pv3': Preset(147, 190).watermark(
    'presets/147x190-pv3.png', 0, 0, 0),
  '150': Preset(150),
  '150x100': Preset(150, 100),
  '160x110-pv3': Preset(160, 110).watermark(
    'presets/160x110-pv3.png', 0, 0, 0),
  # нет картинки '160x120-gb': Preset(160, 120).watermark(
  # 'presets/160x120-gb.png', 0, 0, 0),
  '170-130': Preset(170, 130),
  '178x120': Preset(178, 120),
  '180x128': Preset(180, 128),
  '180x133-pv3': Preset(180, 133).watermark(
    'presets/180x133-pv3.png', 0, 0, 0),
  '200x140-tb': Preset(200, 140).watermark('presets/200x140-tb.png', 0, 0, 0),
  '200x140-zk': Preset(200, 140).watermark('presets/200x140-zk.png', 0, 0, 0),
  '205x140-pv3': Preset(205, 140).watermark(
    'presets/205x140-pv3.png', 0, 0, 0),
  '210x150': Preset(210, 150),
  '220': Preset(220),
  '280x130': Preset(280, 130),
  '295x180-pv3': Preset(295, 180).watermark(
    'presets/295x180-pv3.png', 0, 0, 0),
  '295x180-pv3-zoom': Preset(295, 180).watermark(
    'presets/295x180-pv3-zoom.png', 0, 0, 0),
  '300x200': Preset(300, 200),
  '300x300': Preset(300, 300),
  '370x247': Preset(370, 247),
  '40x30': Preset(40, 30),
  '76x50-pv3': Preset(76, 50).watermark('presets/76x50-pv3.png', 0, 0, 0),
  '800': Preset(800),
  '80x80': Preset(80, 80),
  '93x65-pv3': Preset(93, 65).watermark('presets/93x65-pv3.png', 0, 0, 0),
  '95x95-pv3': Preset(95, 95).watermark('presets/95x95-pv3.png', 0, 0, 0),
  # нет картинки 'l180_s128': Preset(180, 128).watermark(
  #   'presets/l180-s128.png', 0, 0, 0),
  'orig': Preset(1200, 800).no_upscale(),
}
