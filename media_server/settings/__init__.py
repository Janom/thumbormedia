import os

from .base import *  # NOQA

DJANGO_CONF = os.environ.get('DJANGO_CONF', 'default')
if DJANGO_CONF != 'default':
    module = __import__(DJANGO_CONF, globals(), locals(), ['*'])
    overrides = {k: getattr(module, k)
                 for k in dir(module)
                 if not k.startswith('__')}
    locals().update(overrides)

try:
    from local import *  # NOQA
except ImportError:
    pass
