# --coding: utf8--

import os
import sys
from django.conf import global_settings

TEST = 'test' in sys.argv
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def path(*a):
    return os.path.join(PROJECT_ROOT, *a)

sys.path.insert(0, path('apps'))
sys.path.insert(1, path('lib'))
sys.path.insert(1, path('.'))
ADMINS = (
    ('Alex Morozov', 'am@kupo.la'),
)
MANAGERS = ADMINS
ALLOWED_HOSTS = []
TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-ru'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = path('../../media')
MEDIA_URL = '/media/'
STATIC_ROOT = path('../../static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',

)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)
ROOT_URLCONF = 'media_server.urls'
# WSGI_APPLICATION = 'media_client.wsgi.application'
TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)
TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
)
HAML_LOADERS = (('django.template.loaders.cached.Loader',
                 ('hamlpy.template.loaders.HamlPyFilesystemLoader',
                  'hamlpy.template.loaders.HamlPyAppDirectoriesLoader',)),)
COMMON_TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)
TEMPLATE_LOADERS = HAML_LOADERS + COMMON_TEMPLATE_LOADERS
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # vendor
    'rest_framework',
    'djangobower',
    'django_thumbor',
    'django_extensions',
    # custom
    'media_admin',
    'frontend',
    'legacy'
)
BOWER_COMPONENTS_ROOT = path("apps/frontend/static")
BOWER_INSTALLED_APPS = (
    "angular#1.2.16",
    "angular-sanitize#1.3.15",
    "ng-flow#~2",
    "angular-mass-autocomplete#0.2.3",
    "angular-bootstrap#0.12.1",
    "bootstrap-css-only#3.0.0",
)
TEST_RUNNER = 'django.test.runner.DiscoverRunner'
HAML_LOADERS = ('hamlpy.template.loaders.HamlPyFilesystemLoader',
                'hamlpy.template.loaders.HamlPyAppDirectoriesLoader',)
COMMON_TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)
TEMPLATE_LOADERS = HAML_LOADERS + COMMON_TEMPLATE_LOADERS
# настройки загрузки и хранения изображений
FLOWJS_MAX_FILE_SIZE = 10  # max file size (Mb)
FLOWJS_UPLOAD_PATH = MEDIA_ROOT
FLOWJS_JOIN_IN_BACKGROUND = False
FLOWJS_WITH_CELERY = False
FLOWJS_REMOVE_FILES_ON_DELETE = False
