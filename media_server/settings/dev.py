# --coding: utf8--
import os
DEBUG = True
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TMP_OLD_MEDIA = os.path.join(PROJECT_ROOT, '../../files')
HAML_LOADERS = ('hamlpy.template.loaders.HamlPyFilesystemLoader',
                'hamlpy.template.loaders.HamlPyAppDirectoriesLoader',)
COMMON_TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)
TEMPLATE_LOADERS = HAML_LOADERS + COMMON_TEMPLATE_LOADERS

ADMINS = (
    ('jtest', 'anton.s.pilipenko@gmail.com'),
)
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.3.72', '192.168.56.101']
