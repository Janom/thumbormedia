# --coding: utf8--

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect

from rest_framework import routers
from api_v_1.views import ImagesViewSet


def upload_redirect(request):
    u"""редирект на загрузку изображений"""
    url = reverse_lazy('admin:media_admin_uploadimageproxymodel_changelist')
    return HttpResponseRedirect(url)


urlpatterns = patterns(
    '',
    url(r'^$', upload_redirect, name='upload_redirect'),
    url(r'^', include('media_admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('legacy.urls')),
)

api_router = routers.SimpleRouter(trailing_slash=False)
api_router.register(r'images', ImagesViewSet)

urlpatterns += patterns(
    '',
    url(r'^api/v1.0/', include(api_router.urls), ),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
